package com.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.bean.Book;
import com.dao.BookDao;

@Service
public class BookService {
	@Autowired
	BookDao bookDao;
	
	public List<Book> getAllBooks() {
		return bookDao.findAll();
	}
	
	public String storeBookInfo(Book b) {
				
						if(bookDao.existsById(b.getBookid())) {
									return "Book id must be unique";
						}else {
									bookDao.save(b);
									return "Book stored successfully";
						}
	}
	
	public String deleteBookInfo(int bookid) {
		if(!bookDao.existsById(bookid)) {
			return "Book details not present";
			}else {
			bookDao.deleteById(bookid);
			return "Book deleted successfully";
			}	
	}
	
	public String updateBookInfo(Book b) {
		if(!bookDao.existsById(b.getBookid())) {
			return "Book details not present";
			}else {
			Book p	= bookDao.getById(b.getBookid());	// if product not present it will give exception 
			p.setBookprice(b.getBookprice());
			p.setBookname(b.getBookname());// existing product price change 
			bookDao.saveAndFlush(p);				// save and flush method to update the existing product
			return "Book updated successfully";
			}	
	}
	
	public List<Book> findBookUsingBookPrice(float bookprice) {
		return bookDao.getBookByBookPrice(bookprice);
	}
}



