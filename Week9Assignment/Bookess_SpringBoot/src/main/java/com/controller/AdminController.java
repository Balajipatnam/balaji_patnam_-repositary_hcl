package com.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.bean.Admin;

import com.service.AdminService;

@RestController
@RequestMapping("/admin")
public class AdminController {
	@Autowired
	AdminService adminService;
	//http://localhost:9090/admin/Register
	@PostMapping(value = "Register",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeAdminInfo(@RequestBody Admin admin) {
		
				return adminService.storeAdminInfo(admin);
	}
	//http://localhost:9090/admin/updateAdminpassword
	@PatchMapping(value = "updateAdminpassword")
	public String updateAdminInfo(@RequestBody Admin admin) {
					return adminService.updateAdminInfo(admin);
	}
	//http://localhost:9090/admin/login
	@PostMapping(value = "login",
			consumes = MediaType.APPLICATION_JSON_VALUE)
	public String storeAdminLoginInfo(@RequestBody Admin admin) {
		
				return adminService.storeAdminLoginInfo(admin);
	}
	//http://localhost:9090/admin/adminLogOut/
	@GetMapping(value = "adminLogOut/{mailid}")
	public String logout(@PathVariable("mailid") String mailid) {
		return adminService.AdminlogOutInfo(mailid);
	}

	
	
}
	

