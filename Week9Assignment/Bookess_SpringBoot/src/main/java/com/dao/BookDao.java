package com.dao;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.bean.Book;

@Repository
public interface BookDao extends JpaRepository<Book, Integer>{
	@Query("select p from Book p where p.bookprice > :bookprice")
	public List<Book> getBookByBookPrice(@Param("bookprice") float bookprice);

}
