package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookessSpringBootApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookessSpringBootApplication.class, args);
		System.err.println("MY MACHINE RUNNING ON TOMCAT PORT NUMBER 9090");
	}

}
