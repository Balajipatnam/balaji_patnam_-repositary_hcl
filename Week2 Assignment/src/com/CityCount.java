package com;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;

public class CityCount {

	public void cityCount(ArrayList<Employee> emp) {
		
		ArrayList<String> city = new ArrayList<String>();
		
		city.add(emp.get(0).getCity());
		city.add(emp.get(1).getCity());
		city.add(emp.get(2).getCity());
		city.add(emp.get(3).getCity());
		city.add(emp.get(4).getCity());
		
		HashSet<String> hs = new HashSet<String>(city);
	    HashMap<String, Integer> cityMap = new HashMap<String, Integer>();
	 
	    	for (String strElement : hs) {
	    		cityMap.put(strElement,Collections.frequency(city, strElement));
	        }
	    
	    	System.out.println("Count Of Employees From Each City");
	    	System.out.println(""+cityMap);
	 }

}
